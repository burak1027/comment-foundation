FROM openjdk:16-alpine3.13
COPY target/comment-foundation-0.0.1-SNAPSHOT.jar comment-foundation-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/comment-foundation-0.0.1-SNAPSHOT.jar"]
EXPOSE 8081
