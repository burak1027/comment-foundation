//package com.burak.commentfoundation;
//
//import com.burak.commentfoundation.domain.model.dto.CommentDto;
//import com.burak.commentfoundation.domain.repository.CommentDao;
//import com.burak.commentfoundation.domain.repository.CommentDaoImpl;
//import com.burak.commentfoundation.domain.service.CommentMapper;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
//
//
//import java.util.Date;
//
//@ExtendWith(MockitoExtension.class)
//class CommentServiceImplTest {
//
//    @Mock
//    CommentDao commentDao;
//
//    @Test
//    public void createComment(CommentDto commentDto) {
//
//        Date date = new Date();
//        commentDto.setDateCreated(date);
//        commentDto.setConnectedCommentId(commentDto.getCommentId());
//        //Mockito.when(commentDao.createComment(CommentMapper.dtoToEntity(commentDto)));
//
//
//        //commentDao.createComment();
//    }
//
//    public void replyCommentByAComment(CommentDto commentDtoReply, CommentDto commentDto) {
//        Date date = new Date();
//        commentDto.setDateCreated(date);
//        commentDtoReply.setConnectedCommentId(commentDto.getCommentId());
//        commentDao.createComment(CommentMapper.dtoToEntity(commentDtoReply));
//
//    }
//
//    public void replyCommentByAComment(CommentDto commentDto, long id) {
//        commentDao = Mockito.mock(CommentDaoImpl.class);
//        Date date = new Date();
//        commentDto.setDateCreated(date);
//        commentDto.setConnectedCommentId(id);
//        commentDao.createComment(CommentMapper.dtoToEntity(commentDto));
//
//    }
//
//
//    public void deleteComment(CommentDto commentDto) {
//        commentDao.deleteComment(CommentMapper.dtoToEntity(commentDto));
//    }
//
//    public void updateComment(CommentDto commentDto) {
//        commentDao.updateComment(CommentMapper.dtoToEntity(commentDto));
//
//    }
//
//
//    public void deleteCommentByCommentId(long id) {
//        commentDao.deleteCommentById(id);
//    }
//}
