//package com.burak.commentfoundation;
//
//import com.burak.commentfoundation.domain.model.Comment;
//import com.burak.commentfoundation.domain.model.dto.CommentDto;
//import org.junit.Before;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.mockito.junit.jupiter.*;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest
//@SpringJUnitConfig
//@ExtendWith(MockitoExtension.class)
//class CommentFoundationApplicationTests {
//
//    @Test
//    public void contextLoads() {
//        CommentLoadingServiceImplTest commentLoadingServiceImplTest = new CommentLoadingServiceImplTest();
//        for (int i = 0; i < 8; i++) {
//            System.out.println(commentLoadingServiceImplTest.getCommentByCommentId(i).getCommentId());
//
//        }
//
//        long a = commentLoadingServiceImplTest.getCommentByCommentId(1).getCommentId();
//        assertEquals(a, 1);
//        System.out.println(a);
//
//    }
//
//    static List<CommentDto> commentDtoList;
//    static List<Comment> comments;
//
//    //    @Test
//    @BeforeEach
//    public void commentCreation() {
//        CommentServiceImplTest commentService = new CommentServiceImplTest();
//        commentDtoList = new ArrayList<>();
//        Date date = new Date();
//        for (int i = 1; i < 31; i++) {
//            commentDtoList.add(CommentDto.builder().commentId((long) i).commentOwnerId(1L)
//                    .connectedCommentId((long) i).postId(1L).content("heyy").dateCreated(date).dateUpdated(date).build());
//        }
//        for (int i = 1; i < 11; i++) {
//            commentService.replyCommentByAComment(commentDtoList.get((i + 9)), commentDtoList.get((i - 1)).getCommentId());
//        }
//
//        for (int i = 11; i < 21; i++) {
//            commentService.replyCommentByAComment(commentDtoList.get((i + 9)), commentDtoList.get((i - 1)).getCommentId());
//        }
//        System.out.println(commentDtoList.get(18).getConnectedCommentId());
//
//        comments = new ArrayList<>();
//        List<CommentDto> commentDtos = commentDtoList.stream().filter(commentDto -> commentDto.getConnectedCommentId() == commentDto.getCommentId()).collect(Collectors.toList());
//        commentDtos.forEach(commentDto -> comments.add(new Comment(commentDto)));
////        for (CommentDto c:commentDtos) {
////            comments.add(new Comment(c));
////        }
//        comments.add(new Comment(CommentDto.builder().commentId((long) 50).commentOwnerId(1L)
//                .connectedCommentId((long) 50).postId(1L).content("heyy").dateCreated(date).dateUpdated(date).build()));
//        commentMerge(comments);
//        System.out.println(comments);
//        System.out.println(comments.get(7).getChildClasses().get(0).getChildClasses().get(0).getRootClass().getCommentId());
//
//    }
//
//    @Test
//    public void commentsUnderACommentAreNull() {
//        System.out.println(comments.get(3).getRootClass().getCommentId());
//        assertNull(comments.get(comments.size() - 1).getChildClasses());
//    }
//
//    public void commentMerge(List<Comment> comments) {
//        CommentLoadingServiceImplTest commentLoadingService = new CommentLoadingServiceImplTest();
//        for (Comment c : comments) {
//            List<CommentDto> temp = commentLoadingService.getCommentRepliesByCommentId(c.getRootClass().getCommentId(), commentDtoList);
//            if (temp.size() > 0) {
//                //c.setChildClasses(temp);
//                List<Comment> commentReturn = new ArrayList<>();
//                temp.forEach(commentDto ->
//                        commentReturn.add(new Comment(commentDto)));
//                commentMerge(commentReturn);
//                c.setChildClasses(commentReturn);
//            } else {
//
//            }
//
//        }
//        //return  comments;
//    }
//
//}
