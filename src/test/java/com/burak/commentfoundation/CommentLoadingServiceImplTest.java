//package com.burak.commentfoundation;
//
//import com.burak.commentfoundation.domain.model.dto.CommentDto;
//import com.burak.commentfoundation.domain.model.entity.CommentEntity;
//import com.burak.commentfoundation.domain.repository.CommentDao;
//import com.burak.commentfoundation.domain.repository.CommentDaoImpl;
//import com.burak.commentfoundation.domain.service.CommentMapper;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//public class CommentLoadingServiceImplTest {
//    @Mock
//    CommentDaoImpl commentDao;
//    //CommentDao commentDao;
//
//    public CommentDto getCommentByCommentId(long id) {
//        commentDao = Mockito.mock(CommentDaoImpl.class);
//        Date date = new Date();
//        CommentEntity commentEntity = CommentEntity.builder().commentId(id).commentOwnerId(1)
//                .connectedCommentId(1).content("heyy").dateCreated(date).dateUpdated(date).build();
//
//        when(commentDao.getCommentByCommentId(id)).thenReturn(commentEntity);
//        //assertEquals(commentEntity.getCommentId());
//        return CommentMapper.entityToDto(commentDao.getCommentByCommentId(id));
//    }
//
//    @Transactional(readOnly = true)
//
//    public List<CommentDto> getCommentsByPostId(long id) {
//        List<CommentDto> commentDtos = new ArrayList<>();
//        commentDao.getCommentsByPostId(id).forEach(commentEntity ->
//                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
//        return commentDtos;
//    }
//
//    @Transactional(readOnly = true)
//    public List<CommentDto> getCommentsByUserId(long id) {
//        List<CommentDto> commentDtos = new ArrayList<>();
//        commentDao.getCommentsByUserId(id).forEach(commentEntity ->
//                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
//        return commentDtos;
//    }
//
//    @Transactional(readOnly = true)
//    public List<CommentDto> getCommentRepliesByCommentId(long id, List<CommentDto> commentDtoList) {
//        List<CommentDto> commentDtos = new ArrayList<>();
//
//        List<CommentDto> list = commentDtoList.stream().filter(commentDto -> commentDto.getConnectedCommentId() == id && commentDto.getCommentId() != commentDto.getConnectedCommentId())
//                .collect(Collectors.toList());
//        //when(commentDtos).thenReturn(list);
////        commentDao.getCommentRepliesByCommentId(id).forEach(commentEntity ->
////                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
//        commentDtos = list;
//        return commentDtos;
//    }
//
//    @Transactional(readOnly = true)
//    public List<CommentDto> getCommentRepliesByComment(CommentDto commentDto) {
//        List<CommentDto> commentDtos = new ArrayList<>();
//        commentDao.getCommentRepliesByCommentId(commentDto.getCommentId()).forEach(commentEntity ->
//                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
//        return commentDtos;
//    }
//}
