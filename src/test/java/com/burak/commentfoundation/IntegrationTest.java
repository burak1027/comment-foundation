package com.burak.commentfoundation;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.service.CommentLoadingService;
import com.burak.commentfoundation.domain.service.CommentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
//@TestPropertySource("classpath:application-test.properties")
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WebAppConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class IntegrationTest {
    MockMvc mockMvc;
    CommentService commentService;
    CommentLoadingService commentLoadingService;


    @Autowired
    public IntegrationTest(MockMvc mockMvc, CommentService commentService, CommentLoadingService commentLoadingService) {
        this.mockMvc = mockMvc;
        this.commentService = commentService;
        this.commentLoadingService = commentLoadingService;
    }

    public  String asJsonString(CommentDto commentDto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(commentDto);
        return requestJson;

    }
    @Order(1)
    @Test
    void createComments(){
        for (int i = 10; i < 30; i++) {
            CommentDto commentDto = CommentDto.builder().commentId(i).postId(1L).commentOwnerId((long)i).content("It was a really informative paper").build();
            commentService.createComment(commentDto);
        }
        for (int i = 0; i < 10; i++) {
            CommentDto commentDto = CommentDto.builder().commentId((long)i+30).postId(1L).commentOwnerId((long)i).content("It was a really informative paper").build();
            commentService.replyCommentByAComment(commentDto,i);
        }



    }
    @Order(2)
    @Test
    void getCommentById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/comment/get-by-comment-id/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Order(3)
    @Test
    void getCommentByPostId() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/comment/get-comment-by-post-id/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
    @Order(4)
    @Test
    void getCommentsOfAUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/comment//get-comments-by-user-id/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    @Order(5)
    @Test
    void createComment() throws Exception{
        CommentDto commentDto = CommentDto.builder().commentId((long)95).postId(1L).commentOwnerId((long)50).content("It was informative but not detailed. That's why, It didn't meet with my expectations.Also,I recommend author to deep dive into the topics more in her papers.").build();
        mockMvc.perform(MockMvcRequestBuilders.post("/comment").content(asJsonString(commentDto)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
//        commentService.createComment(commentDto);
        assertNotNull(commentLoadingService.getCommentByCommentId(95L));
    }
    @Order(6)
    @Test
    void updateComment() throws Exception{
        CommentDto commentDto = commentLoadingService.getCommentByCommentId(95L);
        commentDto.setContent("It was not fun to read. Also, It didn't meet with my expectations because it is not are of my interests");
        mockMvc.perform(MockMvcRequestBuilders.put("/comment").content(asJsonString(commentDto)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(commentLoadingService.getCommentByCommentId(95L).getContent(),"It was not fun to read. Also, It didn't meet with my expectations because it is not are of my interests");
    }
    @Order(7)
    @Test
    void deleteComment() throws Exception{
        CommentDto commentDto = commentLoadingService.getCommentByCommentId(95L);
        mockMvc.perform(MockMvcRequestBuilders.delete("/comment").content(asJsonString(commentDto)).contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
        assertNull(commentLoadingService.getCommentByCommentId(95L));

    }


}
