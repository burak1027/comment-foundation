package com.burak.commentfoundation;

import com.burak.commentfoundation.domain.model.Comment;
import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.service.CommentLoadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CommentFoundationApplication implements CommandLineRunner {
    @Autowired
    CommentLoadingService commentLoadingService;

    public static void main(String[] args) {
        SpringApplication.run(CommentFoundationApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        List<Comment> postComments = new ArrayList<>();
//        commentLoadingService.getCommentsByPostId(1).forEach(commentDto ->
//                postComments.add(new Comment(commentDto)));
//        List<Comment> commentList = comments(postComments);
//        for (Comment c : postComments ) {
//            //c.setChildClasses(commentLoadingService.getCommentRepliesByCommentId(c.getRootClass().getCommentId()));
//        }
    }

    public List<Comment> comments(List<Comment> comments) {
        for (Comment c : comments) {
            List<CommentDto> temp = commentLoadingService.getCommentRepliesByCommentId(c.getRootClass().getCommentId());
            // c.setChildClasses(temp);
            if (temp != null) {
                List<Comment> commentReturn = new ArrayList<>();
                temp.forEach(commentDto ->
                        commentReturn.add(new Comment(commentDto)));
                comments(commentReturn);
            } else {

            }

        }
        return comments;
    }

}
