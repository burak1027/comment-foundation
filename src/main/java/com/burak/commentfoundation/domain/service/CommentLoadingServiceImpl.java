package com.burak.commentfoundation.domain.service;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.repository.CommentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentLoadingServiceImpl implements CommentLoadingService {
    @Autowired
    CommentDao commentDao;

    @Transactional(readOnly = true)
    @Override
    public CommentDto getCommentByCommentId(long id) {
        return CommentMapper.entityToDto(commentDao.getCommentByCommentId(id));
    }

    @Transactional(readOnly = true)
    @Override
    public List<CommentDto> getCommentsByPostId(long id) {
        List<CommentDto> commentDtos = new ArrayList<>();
        commentDao.getCommentsByPostId(id).forEach(commentEntity ->
                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
        return commentDtos;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CommentDto> getCommentsByUserId(long id) {
        List<CommentDto> commentDtos = new ArrayList<>();
        commentDao.getCommentsByUserId(id).forEach(commentEntity ->
                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
        return commentDtos;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CommentDto> getCommentRepliesByCommentId(long id) {
        List<CommentDto> commentDtos = new ArrayList<>();
        commentDao.getCommentRepliesByCommentId(id).forEach(commentEntity ->
                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
        return commentDtos;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CommentDto> getCommentRepliesByComment(CommentDto commentDto) {
        List<CommentDto> commentDtos = new ArrayList<>();
        commentDao.getCommentRepliesByCommentId(commentDto.getCommentId()).forEach(commentEntity ->
                commentDtos.add(CommentMapper.entityToDto(commentEntity)));
        return commentDtos;
    }
}
