package com.burak.commentfoundation.domain.service;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.model.entity.CommentEntity;
import com.burak.commentfoundation.domain.repository.CommentDao;

import java.util.List;

public interface CommentService {
    void createComment(CommentDto commentDto);

    void replyCommentByAComment(CommentDto commentDtoReply, CommentDto commentDto);

    void replyCommentByAComment(CommentDto commentDto, long id);

    void deleteComment(CommentDto commentDto);

    void updateComment(CommentDto commentDto);

    void deleteCommentByCommentId(long id);

}
