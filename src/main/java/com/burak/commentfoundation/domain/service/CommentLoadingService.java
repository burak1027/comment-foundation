package com.burak.commentfoundation.domain.service;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.model.entity.CommentEntity;

import java.util.List;

public interface CommentLoadingService {
    CommentDto getCommentByCommentId(long id);

    List<CommentDto> getCommentsByPostId(long id);

    List<CommentDto> getCommentsByUserId(long id);


    List<CommentDto> getCommentRepliesByCommentId(long id);

    List<CommentDto> getCommentRepliesByComment(CommentDto commentDto);
}
