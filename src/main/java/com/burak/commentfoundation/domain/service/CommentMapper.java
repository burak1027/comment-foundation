package com.burak.commentfoundation.domain.service;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.model.entity.CommentEntity;

public class CommentMapper {
    public static CommentEntity dtoToEntity(CommentDto commentDto) {
        if(commentDto==null)
            return null;
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setCommentId(commentDto.getCommentId());
        commentEntity.setConnectedCommentId(commentDto.getConnectedCommentId());
        commentEntity.setContent(commentDto.getContent());
        commentEntity.setDateCreated(commentDto.getDateCreated());
        commentEntity.setDateUpdated(commentDto.getDateUpdated());
        commentEntity.setPostId(commentDto.getPostId());
        commentEntity.setCommentOwnerId(commentDto.getCommentOwnerId());
        commentEntity.setDeleted(commentDto.isDeleted());

        return commentEntity;
    }

    public static CommentDto entityToDto(CommentEntity commentEntity) {
        if (commentEntity==null)
            return null;
        CommentDto commentDto = new CommentDto();
        commentDto.setCommentId(commentEntity.getCommentId());
        commentDto.setConnectedCommentId(commentEntity.getConnectedCommentId());
        commentDto.setCommentOwnerId(commentEntity.getCommentOwnerId());
        commentDto.setContent(commentEntity.getContent());
        commentDto.setPostId(commentEntity.getPostId());
        commentDto.setDateCreated(commentEntity.getDateCreated());
        commentDto.setDateUpdated(commentEntity.getDateUpdated());
        commentDto.setDeleted(commentEntity.isDeleted());

        return commentDto;

    }
}
