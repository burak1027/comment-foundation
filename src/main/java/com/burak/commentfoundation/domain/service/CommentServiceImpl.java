package com.burak.commentfoundation.domain.service;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.repository.CommentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentDao commentDao;

    @Transactional
    @Override
    public void createComment(CommentDto commentDto) {
        Date date = new Date();
        commentDto.setDateCreated(date);
        commentDto.setDateUpdated(date);

        commentDto.setConnectedCommentId(commentDto.getCommentId());

        commentDao.createComment(CommentMapper.dtoToEntity(commentDto));
    }
    @Transactional
    @Override
    public void replyCommentByAComment(CommentDto commentDto, long id) {
        Date date = new Date();
        commentDto.setDateCreated(date);
        commentDto.setDateUpdated(date);

        commentDto.setConnectedCommentId(id);
        commentDao.createComment(CommentMapper.dtoToEntity(commentDto));

    }
    @Transactional
    @Override
    public void replyCommentByAComment(CommentDto commentDtoReply, CommentDto commentDto) {
        Date date = new Date();
        commentDto.setDateCreated(date);
        commentDtoReply.setConnectedCommentId(commentDto.getCommentId());
        commentDao.createComment(CommentMapper.dtoToEntity(commentDtoReply));

    }



    @Transactional
    @Override
    public void deleteComment(CommentDto commentDto) {
        commentDao.deleteComment(CommentMapper.dtoToEntity(commentDto));
    }

    @Transactional
    @Override
    public void updateComment(CommentDto commentDto) {
        commentDao.updateComment(CommentMapper.dtoToEntity(commentDto));

    }

    @Transactional
    @Override
    public void deleteCommentByCommentId(long id) {
        commentDao.deleteCommentById(id);
    }
}
