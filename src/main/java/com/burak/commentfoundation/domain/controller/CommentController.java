package com.burak.commentfoundation.domain.controller;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    CommentService commentService;

    @PostMapping
    @ResponseBody
    ResponseEntity<String> createComment(@Valid @RequestBody CommentDto commentDto) {
        commentService.createComment(commentDto);
        return ResponseEntity.ok("comment is valid");
    }

    @PostMapping("/reply-comment/{value}")
    @ResponseBody
    ResponseEntity<String> replyToAComment(@Valid @RequestBody CommentDto commentDto,@Positive @PathVariable("value") long id) {
        commentService.replyCommentByAComment(commentDto, id);
        return ResponseEntity.ok("reply is valid");
    }

    @DeleteMapping
    @ResponseBody
    ResponseEntity<String> deleteComment(@Valid @RequestBody CommentDto commentDto) {
        commentService.deleteComment(commentDto);
        return ResponseEntity.ok("deletion is valid");
    }

    @DeleteMapping("/{value}")
    @ResponseBody
    ResponseEntity<String> deleteCommentById(@Positive @PathVariable("value") long id) {
        commentService.deleteCommentByCommentId(id);
        return ResponseEntity.ok("valid deletion");
    }

    @PutMapping
    @ResponseBody
    ResponseEntity<String> updateComment(@Valid @RequestBody CommentDto commentDto) {
        commentService.updateComment(commentDto);
        return ResponseEntity.ok("update is valid");
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
