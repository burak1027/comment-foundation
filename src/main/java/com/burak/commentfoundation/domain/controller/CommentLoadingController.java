package com.burak.commentfoundation.domain.controller;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import com.burak.commentfoundation.domain.service.CommentLoadingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/comment")
@Api(value = "commentLoading", produces = MediaType.APPLICATION_JSON_VALUE)

public class CommentLoadingController {

    @Autowired
    CommentLoadingService commentLoadingService;

    @GetMapping("/get-by-comment-id/{value}")
    CommentDto getCommentByCommentId(@Positive @PathVariable("value") long id) {
        return commentLoadingService.getCommentByCommentId(id);
    }

    @GetMapping("/get-comment-by-post-id/{value}")
    List<CommentDto> getCommentsOfAPost(@Positive @PathVariable("value") long id) {
        return commentLoadingService.getCommentsByPostId(id);
    }

    @GetMapping("/get-comments-by-user-id/{value}")
    List<CommentDto> getCommentsOfAUser(@Positive @PathVariable("value") long id) {
        return commentLoadingService.getCommentsByUserId(id);
    }

    @GetMapping("/get-comment-replies/{value}")
    List<CommentDto> getRepliesOfAComment(@Positive @PathVariable("value") long id) {
        return commentLoadingService.getCommentRepliesByCommentId(id);
    }

}
