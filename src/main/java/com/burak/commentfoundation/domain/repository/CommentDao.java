package com.burak.commentfoundation.domain.repository;

import com.burak.commentfoundation.domain.model.entity.CommentEntity;

import java.util.List;

public interface CommentDao {
    CommentEntity getCommentByCommentId(long id);

    List<CommentEntity> getCommentsByPostId(long id);

    List<CommentEntity> getCommentsByUserId(long id);

    List<CommentEntity> getCommentRepliesByCommentId(long id);

    List<CommentEntity> getCommentRepliesByComment(CommentEntity commentEntity);


    void createComment(CommentEntity commentEntity);

    void updateComment(CommentEntity commentEntity);

    void deleteComment(CommentEntity commentEntity);

    void deleteCommentById(long id);
}
