package com.burak.commentfoundation.domain.repository;

import com.burak.commentfoundation.domain.model.entity.CommentEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CommentDaoImpl implements CommentDao {
    @Autowired
    private EntityManager entityManager;

    @Override
    public CommentEntity getCommentByCommentId(long id) {
        Session session = entityManager.unwrap(Session.class);
        return session.get(CommentEntity.class, id);
    }

    @Override
    public List<CommentEntity> getCommentsByPostId(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM CommentEntity WHERE postId = :param");
        query.setParameter("param", id);
        return query.list();
    }

    @Override
    public List<CommentEntity> getCommentsByUserId(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM CommentEntity WHERE commentOwnerId = :param");
        query.setParameter("param", id);
        return query.list();
    }

    @Override
    public List<CommentEntity> getCommentRepliesByCommentId(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM CommentEntity WHERE postId = :param and commentId != :param");
        query.setParameter("param", id);
        List<CommentEntity> commentEntityList = query.list();
        return commentEntityList;
    }

    @Override
    public List<CommentEntity> getCommentRepliesByComment(CommentEntity commentEntity) {

        return null;
    }

    @Override
    public void createComment(CommentEntity commentEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.save(commentEntity);
    }

    @Override
    public void updateComment(CommentEntity commentEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.update(commentEntity);
    }

    @Override
    public void deleteComment(CommentEntity commentEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.remove(session.contains(commentEntity) ? commentEntity : session.merge(commentEntity));

    }

    @Override
    public void deleteCommentById(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("UPDATE FROM CommentEntity SET deleted = true WHERE postId = :param");
        query.setParameter("param", id);
        query.executeUpdate();
    }
}
