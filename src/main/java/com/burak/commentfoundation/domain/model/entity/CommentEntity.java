package com.burak.commentfoundation.domain.model.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "comment")
@SQLDelete(sql = "UPDATE comment SET deleted = true WHERE comment_id=?")
@Where(clause = "deleted=false")
public class CommentEntity {
    @Id
   // @GeneratedValue
    @Column(name = "comment_id")
    private long commentId;
    @Column(name = "comment_user_id")
    private long commentOwnerId;
    @Column(name = "replied_comment_id")
    private long connectedCommentId;
    @Column(name = "post_id")
    private long postId;
    @Column(name = "date_created")
    @Type(type = "date")
    private Date dateCreated;
    @Column(name = "date_updated")
    @Type(type = "date")
    private Date dateUpdated;
    @Column(name = "content")
    private String content;
    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;

}
