package com.burak.commentfoundation.domain.model.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CommentDto {
    @NotNull
    long commentId;
    @NotNull
    long commentOwnerId;
    //@NotNull
    long connectedCommentId;
    @NotNull
    long postId;
    //    @DateTimeFormat("yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "2021-05-21")
    @PastOrPresent
    Date dateCreated;
    @ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "2021-05-21")
    @PastOrPresent
    Date dateUpdated;
    @Size(min = 1, max = 2000)
    String content;
    @AssertFalse
    boolean deleted;



}
