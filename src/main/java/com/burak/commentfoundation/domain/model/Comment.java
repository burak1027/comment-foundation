package com.burak.commentfoundation.domain.model;

import com.burak.commentfoundation.domain.model.dto.CommentDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Comment {
    CommentDto rootClass;
    List<Comment> childClasses;

    public Comment(CommentDto commentDto) {
        rootClass = commentDto;
    }

    @Override
    public String toString() {
        String com = "Root Class id " + rootClass.getCommentId() + "\n";
        if (childClasses != null) {
            for (Comment x : childClasses) {
                com += x.getRootClass().getCommentId() + " is Child class of " + rootClass.getCommentId() + "\n";
            }
        } else {
            com += "has not child \n";
        }
        com += "---------- \n";

        return com;
    }
}
